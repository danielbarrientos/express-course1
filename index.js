const express = require('express')

const app = express()

/** Settings */
app.set('appName','Express tutorial')
app.set('PORT',4000)

app.set('view engine','ejs')
/** Middlewares */

function logger(req,res,next) {   
    console.log(`request received ${req.method}: ${req.protocol}://${req.get('host')}${req.originalUrl}`)
    next()
}
app.use(express.json());
app.use(logger);
//


/** Routes */

app.get('/', (req,res) => {
    const data= [{name:'jhon'},{name:"cameron"},{name:'mavil'},{name:'juanito'}]
    res.render('index.ejs', {data})
})
// app.all('/user',(req,res, next) => {
//     console.log('I pass by here')
//     // res.send('finish');
//     next()
// })

app.get('/user',(req,res) => {
    res.send('hello world');
})

app.post('/user/:id',(req,res) => {

     res.json({
         ok:true,
         req:req.body,
         params:req.params,
         message:"created"
     });
})
app.put('/user/:id',(req,res) => {

    res.json({
        ok:true,
        req:req.body,
        params:req.params,
        message:"updated"
    });
})

app.delete('/user/:id',(req,res) => {

    res.json({
        ok:true,
        req:req.body,
        params:req.params,
        message:"deleted"
    });
})

app.use(express.static(__dirname+'/public'))

app.listen(app.get('PORT'), () => {
    console.log(app.get('appName')),
    console.log(`Server started on port ${app.get('PORT')}`);
});